# Add exenv to the env for easy switching and installing of elixir versions
export PATH="$HOME/.exenv/bin:$PATH"

# Initialises and refreshes exenv path shims
eval "$(exenv init -)"
